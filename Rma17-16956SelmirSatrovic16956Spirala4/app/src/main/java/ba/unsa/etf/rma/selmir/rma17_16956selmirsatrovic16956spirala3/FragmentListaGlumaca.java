package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.ParseException;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Salva on 18-May-17.
 */

public class FragmentListaGlumaca extends Fragment implements SkiniPodatkeGlumcaResiver.Receiver {
    private ArrayList<Glumac> glumci;
    public static AdapterGlumac glumacAdapter;
    private onItemClick oic;
    private SkiniPodatkeGlumcaResiver gResiver;
    ListView lv;
/*
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments().containsKey("Alista")) {
            glumci2 = getArguments().getParcelableArrayList("Alista");

            lv = (ListView) getView().findViewById(R.id.listViewGlumci);

            AdapterGlumac aa ;
            aa=new AdapterGlumac(getActivity());
            aa.addGlumacList(glumci2);
            lv.setAdapter(aa);
            aa.notifyDataSetChanged();

        }
    }*/


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        glumci=GlumacPomocna.getInstance().getGlumci();
        glumacAdapter=new AdapterGlumac(getActivity());
        glumacAdapter.addGlumacList(glumci);

        View nv=inflater.inflate(R.layout.fragment_lista_glumaca,container,false);



        final EditText unoseditText=(EditText)nv.findViewById(R.id.unosEditText);
        Button dugmeTrazi=(Button)nv.findViewById(R.id.dugmeTrazi);
        ListView listView=(ListView)nv.findViewById(R.id.listViewGlumci);
        listView.setFocusable(true);
        listView.setAdapter(glumacAdapter);
        glumacAdapter.notifyDataSetChanged();

        unosTekstaIntent(unoseditText);

        dugmeTrazi.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                GlumacPomocna.getInstance().getGlumci().clear();
                glumacAdapter.notifyDataSetChanged();

                if(unoseditText.getText().length()<6 ||(unoseditText.getText().length()>6 && !unoseditText.getText().toString().toLowerCase().contains("actor:")&&!unoseditText.getText().toString().toLowerCase().contains("director:")))
                {
                    FragmentDetaljiGlumac.jelIzBaze=false;
                    Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), pretragaThemoviedbService.class);
                    gResiver=new SkiniPodatkeGlumcaResiver(new Handler());
                    gResiver.setReceiver(FragmentListaGlumaca.this);
                    intent.putExtra("query",unoseditText.getText().toString());
                    intent.putExtra("resiver",gResiver);
                    getActivity().startService(intent);
                }
                else if(unoseditText.getText().toString().toLowerCase().contains("director:"))
                {
                    FragmentDetaljiGlumac.jelIzBaze=true;
                    String unos=unoseditText.getText().toString().substring(9);
                    Baza b=new Baza(getActivity());
                    b.DajGlumceReziser(unos);
                }
                else //if(unoseditText.getText().toString().substring(0,5).toLowerCase().equals("actor:"))
                {
                    FragmentDetaljiGlumac.jelIzBaze=true;
                    String unos=unoseditText.getText().toString().substring(6);
                    Baza b=new Baza(getActivity());
                    b.DajGlumce(unos);
                }





                unoseditText.setText("");
                View view=getActivity().getCurrentFocus();


                if(view!=null)
                {
                    InputMethodManager imm=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(),0);
                }


            }
        });

        try
        {
            oic = (onItemClick)getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + "Treba implementirati OnItemClick");
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                oic.onItemClicked(position);
            }
        });


        return nv;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case pretragaThemoviedbService.STATUS_FINISHED:
                glumacAdapter.notifyDataSetChanged();
                break;
            case pretragaThemoviedbService.STATUS_ERROR:
                Toast.makeText(getActivity(), "ERROR", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void unosTekstaIntent(TextView editText) {
        Intent intent = getActivity().getIntent();
        if (intent.getAction().matches(Intent.ACTION_SEND)) {
            editText.setText(intent.getStringExtra(Intent.EXTRA_TEXT));
        }
    }

    public interface onItemClick
    {
        public void onItemClicked(int pos);
    }
}
