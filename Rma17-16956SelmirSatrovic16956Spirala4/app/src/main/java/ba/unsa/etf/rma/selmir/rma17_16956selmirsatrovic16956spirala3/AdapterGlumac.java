package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Salva on 18-May-17.
 */

public class AdapterGlumac extends BaseAdapter
{

    private Context mainActivity;

    List<Glumac> glumacList=new ArrayList<Glumac>();

    public AdapterGlumac(Context main){mainActivity=main;}

    @Override
    public int getCount(){return glumacList.size();}
    @Override
    public Glumac getItem(int position){return glumacList.get(position);}
    @Override
    public long getItemId(int position){return  position;}
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView==null)
        {
            LayoutInflater inflater=(LayoutInflater)mainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.element_liste_glumci,parent,false);
        }

        ImageView slika = (ImageView)convertView.findViewById(R.id.Slika);
        TextView naziv = (TextView)convertView.findViewById(R.id.textViewIme);
        TextView godina = (TextView)convertView.findViewById(R.id.textViewGodina);
        TextView grad=(TextView)convertView.findViewById(R.id.textViewGrad);
        TextView rating=(TextView)convertView.findViewById(R.id.textViewRating);

        Glumac tmpGlumac=glumacList.get(position);

        naziv.setText(tmpGlumac.getIme());
        godina.setText(tmpGlumac.getGodinaRodjenja());
        grad.setText(tmpGlumac.getGradRodjenja());
        rating.setText("Rejting : "+tmpGlumac.getRating());

        slika.setImageBitmap(GlumacPomocna.getInstance().getGlumci().get(position).getSlikaGlumca());
           // slika.setImageResource(R.drawable.prazno);
        return convertView;
    }

    public void addGlumac(Glumac g)
    {
       glumacList.add(g);
       // notifyDataSetChanged();
    }

    public void addGlumacList(ArrayList<Glumac> g)
    {
        glumacList=g;
    }
}