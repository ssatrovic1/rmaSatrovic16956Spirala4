package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Salva on 22-May-17.
 */

public class FragmentListaRezisera extends Fragment {

    private ArrayList<Reziser> reziseri;
    ListView lv;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragmet_lista_rezisera,container,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if(getArguments().containsKey("Rlista"))
        {
            reziseri=getArguments().getParcelableArrayList("Rlista");

            lv=(ListView)getView().findViewById(R.id.list_viewReziseri);

            AdapterReziser aa=new AdapterReziser(getActivity(),R.layout.element_liste_reziser,reziseri);
            lv.setAdapter(aa);

        }



    }


}