package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.os.ResultReceiver;

/**
 * Created by Salva on 18-May-17.
 */

public class pretragaThemoviedbService extends IntentService {

    public static final int STATUS_RUNNING = 1;
    public static final int STATUS_FINISHED = 2;
    public static final int STATUS_ERROR = 3;

    public pretragaThemoviedbService(){super(null);}

    public pretragaThemoviedbService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final android.os.ResultReceiver resultReceiver =  intent.getParcelableExtra("resiver");
        String searchquery=intent.getStringExtra("query");
        Bundle bundle=new Bundle();
        resultReceiver.send(STATUS_RUNNING,Bundle.EMPTY);

        try
        {
            SkiniPodatkeGlumca.DajGlumca(searchquery);
            resultReceiver.send(STATUS_FINISHED,Bundle.EMPTY);
        }
        catch (Exception e){
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            resultReceiver.send(STATUS_ERROR, bundle);
        }

    }
}
