package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Salva on 22-May-17.
 */


public class AdapterReziser extends ArrayAdapter<Reziser> {


    private int _resource;
    public AdapterReziser(Context context, int resource, List<Reziser> objects) {
        super(context, resource, objects);
        _resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LinearLayout newView;

        if(convertView == null)
        {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li = (LayoutInflater)getContext().getSystemService(inflater);
            li.inflate(_resource, newView, true);
        }
        else
        {
            newView = (LinearLayout)convertView;
        }


        Reziser reziser=getItem(position);
        TextView naziv = (TextView)newView.findViewById(R.id.textViewNazivReziser);
        naziv.setText(reziser.getIme());





        return newView;
    }

}