package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Salva on 18-May-17.
 */

public class Zanr implements Parcelable {

    private String ime;

    public Zanr(){}
    public Zanr(String ime) {
        this.ime = ime;
    }

    protected Zanr(Parcel in)
    {
        ime=in.readString();
    }

    public static final Creator<Zanr> CREATOR = new Creator<Zanr>() {
        @Override
        public Zanr createFromParcel(Parcel in) {
            return new Zanr(in);
        }

        @Override
        public Zanr[] newArray(int size) {
            return new Zanr[size];
        }
    };

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    @Override
    public  int describeContents()
    {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest,int flags)
    {
        dest.writeString(ime);
    }

}