package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Salva on 18-May-17.
 */

public class Glumac implements Parcelable {

    private String gTheMoviedbID;



    private String ime;
    private String godinaRodjenja;
    private String godinaSmrti;
    private String mjestoRodjenja;
    private String rating;
    private String biografija;
    private String link;
    private String spol;
    private Bitmap slikaGlumca;




    //konstruktor
    public Glumac(){}
    public Glumac(String ime, String godinaRodjenja, String godinaSmrti, String mjestoRodjenja, String rating, String biografija, String link,String spol) {
        this.ime = ime;
        this.godinaRodjenja = godinaRodjenja;
        this.godinaSmrti = godinaSmrti;
        this.mjestoRodjenja = mjestoRodjenja;
        this.rating = rating;
        this.biografija = biografija;
        this.link = link;
        this.spol=spol;
    }

    protected Glumac(Parcel in)
    {
        ime=in.readString();
        godinaRodjenja=in.readString();
        godinaSmrti=in.readString();
        mjestoRodjenja=in.readString();
        rating=in.readString();
        biografija=in.readString();
        link=in.readString();
        spol=in.readString();
    }

    public static final Creator<Glumac> CREATOR = new Creator<Glumac>() {
        @Override
        public Glumac createFromParcel(Parcel in) {
            return new Glumac(in);
        }

        @Override
        public Glumac[] newArray(int size) {
            return new Glumac[size];
        }
    };

    //geteri seteri



    public Bitmap getSlikaGlumca() {
        return slikaGlumca;
    }

    public void setSlikaGlumca(Bitmap slikaGlumca) {
        this.slikaGlumca = slikaGlumca;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getgTheMoviedbID() {
        return gTheMoviedbID;
    }

    public void setgTheMoviedbID(String gTheMoviedbID) {
        this.gTheMoviedbID = gTheMoviedbID;
    }

    public String getGodinaRodjenja() {
        return godinaRodjenja;
    }

    public void setGodinaRodjenja(String godinaRodjenja) {
        this.godinaRodjenja = godinaRodjenja;
    }

    public String getGradRodjenja() {
        return mjestoRodjenja;
    }

    public void setGradRodjenja(String gradRodjenja) {
        this.mjestoRodjenja = gradRodjenja;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getGodinaSmrti() {
        return godinaSmrti;
    }

    public void setGodinaSmrti(String godinaSmrti) {
        this.godinaSmrti = godinaSmrti;
    }

    public String getMjestoRodjenja() {
        return mjestoRodjenja;
    }

    public void setMjestoRodjenja(String mjestoRodjenja) {
        this.mjestoRodjenja = mjestoRodjenja;
    }

    public String getBiografija() {
        return biografija;
    }

    public void setBiografija(String biografija) {
        this.biografija = biografija;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSpol() {
        return spol;
    }

    public void setSpol(String spol) {
        this.spol = spol;
    }

    @Override
    public  int describeContents()
    {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest,int flags)
    {
        dest.writeString(ime);
        dest.writeString(godinaRodjenja);
        dest.writeString(godinaSmrti);
        dest.writeString(mjestoRodjenja);
        dest.writeString(rating);
        dest.writeString(biografija);
        dest.writeString(link);
        dest.writeString(spol);
    }


}