package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Salva on 18-May-17.
 */

public class FragmentTriDugmeta extends Fragment {


    private AdapterGlumac glumacAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view=inflater.inflate(R.layout.fragment_tri_buttona,container,false);

        Button buttonglumci;
        Button buttonreziseri;
        Button buttonzanrovi;

        ImageView slikazastave=(ImageView)view.findViewById(R.id.imageViewZastava);
        if(Locale.getDefault().getLanguage().toLowerCase().contains("en"))
            slikazastave.setImageResource(R.drawable.en);
        else
            slikazastave.setImageResource(R.drawable.bih);

        buttonglumci=(Button)view.findViewById(R.id.buttonGlumci);
        buttonreziseri=(Button)view.findViewById(R.id.buttonReziseri);
        buttonzanrovi=(Button)view.findViewById(R.id.buttonZanrovi);

        buttonglumci.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                FragmentManager fm=getFragmentManager();
                FragmentListaGlumaca fl=new FragmentListaGlumaca();
                Bundle argumenti=new Bundle();


                ArrayList<Glumac> glumci=GlumacPomocna.getInstance().getGlumci();
                argumenti.putParcelableArrayList("Alista",glumci);
                fl.setArguments(argumenti);
                fm.beginTransaction().replace(R.id.mjestoGlumci,fl,"Lista").addToBackStack(null).commit();


            }


        });

        buttonzanrovi.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                FragmentManager fm=getFragmentManager();
                FragmentListaZanrova fl=new FragmentListaZanrova();
                Bundle argumenti=new Bundle();
                ArrayList<Glumac> glumci=GlumacPomocna.getInstance().getGlumci();
                if(Pocetni.odabraniGlumacPozicija==-1)
                {
                    ArrayList<Zanr> zanrovi = new ArrayList<Zanr>();
                    argumenti.putParcelableArrayList("Zlista",zanrovi);
                    fl.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.mjestoGlumci,fl,"Lista").addToBackStack(null).commit();
                }
                else
                {


                    ArrayList<Zanr> zanrovi = FragmentDetaljiGlumac.listaZanrova;
                    argumenti.putParcelableArrayList("Zlista",zanrovi);
                    fl.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.mjestoGlumci,fl,"Lista").addToBackStack(null).commit();
                }

            }


        });

        buttonreziseri.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                FragmentManager fm=getFragmentManager();
                FragmentListaRezisera fl=new FragmentListaRezisera();
                Bundle argumenti=new Bundle();
                ArrayList<Glumac> glumci=GlumacPomocna.getInstance().getGlumci();
                if(Pocetni.odabraniGlumacPozicija==-1)
                {
                    ArrayList<Reziser> reziseri = new ArrayList<Reziser>();
                    argumenti.putParcelableArrayList("Rlista",reziseri);
                    fl.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.mjestoGlumci,fl,"Lista").addToBackStack(null).commit();
                }
                else
                {

                    ArrayList<Reziser> reziseri = FragmentDetaljiGlumac.listaRezisera;
                    argumenti.putParcelableArrayList("Rlista",reziseri);
                    fl.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.mjestoGlumci,fl,"Lista").addToBackStack(null).commit();

                }
            }


        });


        return view;



    }


}
