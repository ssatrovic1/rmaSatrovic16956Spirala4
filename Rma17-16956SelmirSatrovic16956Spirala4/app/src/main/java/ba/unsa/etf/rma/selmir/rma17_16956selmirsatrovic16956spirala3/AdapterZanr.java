package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Salva on 22-May-17.
 */

public class AdapterZanr extends ArrayAdapter<Zanr> {

    private int _resource;
    public AdapterZanr(Context context, int resource, List<Zanr> objects) {
        super(context, resource, objects);
        _resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LinearLayout newView;

        if(convertView == null)
        {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li = (LayoutInflater)getContext().getSystemService(inflater);
            li.inflate(_resource, newView, true);
        }
        else
        {
            newView = (LinearLayout)convertView;
        }


        Zanr zanr=getItem(position);
        ImageView slika = (ImageView)newView.findViewById(R.id.SlikaZanr);
        TextView naziv = (TextView)newView.findViewById(R.id.textViewImeZanr);
        naziv.setText(zanr.getIme());


        if(zanr.getIme().toLowerCase().contains("horror"))
        {
            slika.setImageResource(R.drawable.horor);
        }
        else if(zanr.getIme().toLowerCase().contains("action"))
        {
            slika.setImageResource(R.drawable.akcija);
        }
        else if(zanr.getIme().toLowerCase().contains("drama"))
        {
            slika.setImageResource(R.drawable.drama);
        }
        else if(zanr.getIme().toLowerCase().contains("thriller"))
        {
            slika.setImageResource(R.drawable.triler);
        }
        else if(zanr.getIme().toLowerCase().contains("comedy"))
        {
            slika.setImageResource(R.drawable.komedija);
        }

        else if(zanr.getIme().toLowerCase().contains("documentary"))
        {
            slika.setImageResource(R.drawable.doc);
        }
        else if(zanr.getIme().toLowerCase().contains("crime"))
        {
            slika.setImageResource(R.drawable.crime);
        }
        else if(zanr.getIme().toLowerCase().contains("western"))
        {
            slika.setImageResource(R.drawable.west);
        }
        else if(zanr.getIme().toLowerCase().contains("adventure"))
        {
            slika.setImageResource(R.drawable.adventure);
        }
        else if(zanr.getIme().toLowerCase().contains("fantasy"))
        {
            slika.setImageResource(R.drawable.fantasy);
        }
        else if(zanr.getIme().toLowerCase().contains("music"))
        {
            slika.setImageResource(R.drawable.music);
        }
        else if(zanr.getIme().toLowerCase().contains("fiction"))
        {
            slika.setImageResource(R.drawable.sf);
        }
        else
        {
            slika.setImageResource(R.drawable.prazno);
        }


        return newView;
    }

}
