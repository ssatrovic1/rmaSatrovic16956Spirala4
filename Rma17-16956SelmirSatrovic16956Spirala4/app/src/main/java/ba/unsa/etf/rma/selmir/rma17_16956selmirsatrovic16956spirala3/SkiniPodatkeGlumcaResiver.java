package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by Salva on 18-May-17.
 */

public class SkiniPodatkeGlumcaResiver extends ResultReceiver {
    private Receiver gReceiver;


    public SkiniPodatkeGlumcaResiver(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver recevier)
    {
        gReceiver = recevier;
    }

    public interface Receiver
    {
        public void onReceiveResult(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData)
    {
        if(gReceiver != null) {
            gReceiver.onReceiveResult(resultCode, resultData);
        }

    }

}