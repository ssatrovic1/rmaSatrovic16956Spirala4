package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Salva on 22-May-17.
 */

public class FragmentListaZanrova extends Fragment{
    private ArrayList<Zanr> zanrovi;
    ListView lv;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_lista_zanrova,container,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if(getArguments().containsKey("Zlista"))
        {
            zanrovi=getArguments().getParcelableArrayList("Zlista");

            lv=(ListView)getView().findViewById(R.id.list_viewZanrovif);

            AdapterZanr aa=new AdapterZanr(getActivity(),R.layout.element_liste_zanr,zanrovi);
            lv.setAdapter(aa);

        }



    }
}
