package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import android.os.Handler;

/**
 * Created by Salva on 18-May-17.
 */
class MyThread extends Thread
{
    Handler handler;
    public MyThread()
    {

    }
    @Override
    public void run()
    {
        Looper.prepare();
        handler=new Handler();
        Looper.loop();
    }
}


public class FragmentDetaljiGlumac extends Fragment {

    public static boolean zanr=false;
    public static boolean reziser=false;
    public static boolean jelIzBaze=false;
    public static ArrayList<Zanr> listaZanrova=new ArrayList<Zanr>();
    public static ArrayList<Reziser> listaRezisera=new ArrayList<Reziser>();

    Activity act;

    Glumac izabraniGlumac=new Glumac();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        Thread TmpTred=null;
        Thread tmpTred=null;
        View iv=inflater.inflate(R.layout.fragment_detalji_glumca,container,false);
        final Button dugmeBookmark=(Button)iv.findViewById(R.id.dugmeBookmark);
        TextView naziv=(TextView)iv.findViewById(R.id.textViewNaziv);
        TextView spol=(TextView)iv.findViewById(R.id.textViewSpol);
        TextView website=(TextView)iv.findViewById(R.id.textViewImdbLink);
        TextView rodjSmr=(TextView)iv.findViewById(R.id.textViewGodinaRodjenjaSmrti);
        TextView mjestoRodj=(TextView)iv.findViewById(R.id.textViewMjestoRodjenja);
        final TextView bio=(TextView)iv.findViewById(R.id.textViewBiografija);
        ImageView sl=(ImageView)iv.findViewById(R.id.slikaBio);

        if(getArguments() != null && getArguments().containsKey("Izabrani")){

            int izabranaPozicija = (int)getArguments().get("Izabrani");
            if(GlumacPomocna.getInstance().getGlumci().size() != 0) {
                izabraniGlumac = GlumacPomocna.getInstance().getGlumci().get(izabranaPozicija);
               // int duzina=GlumacPomocna.getInstance().getGlumci().size();

                final String glumac_ID=izabraniGlumac.getgTheMoviedbID();
                if(jelIzBaze==false)
                {
                    try
                    {
                        TmpTred= new Thread(new Runnable()
                        {
                            @Override
                            public void run()
                            {

                                //dugmeBookmark.setEnabled(false);
                                SkiniPodatkeGlumca.DajZanroveZaGlumca(glumac_ID);
                                //dugmeBookmark.setEnabled(false);
                                zanr=true;
                            }
                        });
                        TmpTred.start();


                    }catch (Exception c) { Log.i("ERROR", c.getMessage()); }
                    try
                    {
                         tmpTred=new Thread(new Runnable()
                        {
                            @Override
                            public void run()
                            {

                                //dugmeBookmark.setEnabled(false);
                                SkiniPodatkeGlumca.DajRezisereZaGlumca(glumac_ID);
                                //dugmeBookmark.setEnabled(true);
                                reziser=true;
                            }
                        });
                        tmpTred.start();
                    }catch (Exception c) { Log.i("ERROR", c.getMessage()); }








                }
                else
                {
                    Baza b=new Baza(getActivity());
                    b.postaviZanroveRezisere(glumac_ID);
                }


                naziv.setText(izabraniGlumac.getIme());
                spol.setText(izabraniGlumac.getSpol());
                website.setText(izabraniGlumac.getLink());
                bio.setText(izabraniGlumac.getBiografija());
                rodjSmr.setText(izabraniGlumac.getGodinaRodjenja()+"-"+izabraniGlumac.getGodinaSmrti());
                mjestoRodj.setText("  "+izabraniGlumac.getMjestoRodjenja());

                sl.setImageBitmap(GlumacPomocna.getInstance().getGlumci().get(izabranaPozicija).getSlikaGlumca());
               // sl.setImageResource(R.drawable.prazno);
                //boja pozadine
                ScrollView glavniLayout=(ScrollView)iv.findViewById(R.id.prikaz);
                if(spol.getText().toString().toLowerCase().contains("muski")||spol.getText().toString().toLowerCase().contains("male"))
                    glavniLayout.setBackgroundColor(Color.parseColor("#BDEDFF"));
                else
                    glavniLayout.setBackgroundColor(Color.parseColor("#FED6FB"));


                //spinner.setVisibility(View.VISIBLE);
                //while(TmpTred.isAlive()&&tmpTred.isAlive()){

                   // spinner.setVisibility(View.VISIBLE);
                //}
                //dugmeBookmark.setEnabled(true);


            }

        }


        // link glumca klik
        final TextView web=(TextView)iv.findViewById(R.id.textViewImdbLink);

        web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url ="http://"+ (String) web.getText();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        //dugme dijeli
        Button dugmeDijeli=(Button)iv.findViewById(R.id.dugmeDijeli);

        dugmeDijeli.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, bio.getText());
                sendIntent.setType("text/plain");
// Provjera da li postoji aplikacija koja može obaviti navedenu akciju
                if (sendIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(sendIntent);
                }
            }

        });

        //dugme bookmark
if(reziser==true&&zanr==true)
    dugmeBookmark.setEnabled(true);

        dugmeBookmark.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view) {

                Baza b=new Baza(getActivity());
                b.DodajGlumca(izabraniGlumac);
            }

        });

        //dugme ukloni bukmark
        Button dugmeUkloniBookmark=(Button)iv.findViewById(R.id.dugmeUnBookmark);

        dugmeUkloniBookmark.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view) {

                Baza b=new Baza(getActivity());
                b.UkloniGlumca(izabraniGlumac);
            }

        });

        return iv;
    }

}
