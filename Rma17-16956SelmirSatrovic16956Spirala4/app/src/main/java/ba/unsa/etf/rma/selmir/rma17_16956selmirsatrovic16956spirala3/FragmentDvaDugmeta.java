package ba.unsa.etf.rma.selmir.rma17_16956selmirsatrovic16956spirala3;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

/**
 * Created by Salva on 18-May-17.
 */

public class FragmentDvaDugmeta extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view=inflater.inflate(R.layout.fragment_dva_butona,container,false);

        Button buttonglumci;
        Button buttonostalo;

        buttonglumci=(Button)view.findViewById(R.id.buttonGlumci);
        buttonostalo=(Button)view.findViewById(R.id.dugmeOstalo);

        buttonglumci.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                FragmentManager fm=getFragmentManager();
                FragmentListaGlumaca fl=new FragmentListaGlumaca();
                Bundle argumenti=new Bundle();


                ArrayList<Glumac> glumci=GlumacPomocna.getInstance().getGlumci();
                argumenti.putParcelableArrayList("Alista",glumci);
                fl.setArguments(argumenti);
                fm.beginTransaction().replace(R.id.mjestoGlumci,fl,"Lista").addToBackStack(null).commit();

                argumenti=new Bundle();
                argumenti.putInt("Izabrani", 0);
                FragmentDetaljiGlumac fd=new FragmentDetaljiGlumac();
                fd.setArguments(argumenti);
                fm.beginTransaction().replace(R.id.mjestoOstalo, fd, "Detalji").commit();

            }


        });

        buttonostalo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                FragmentManager fm=getFragmentManager();
                FragmentListaRezisera fl=new FragmentListaRezisera();
                FragmentListaZanrova flz=new FragmentListaZanrova();
                Bundle argumenti=new Bundle();
                ArrayList<Glumac> glumci=GlumacPomocna.getInstance().getGlumci();
                if(Pocetni.odabraniGlumacPozicija==-1)
                {
                    ArrayList<Reziser> reziseri = new ArrayList<Reziser>();
                    ArrayList<Zanr> zanrovi=new ArrayList<Zanr>();
                    argumenti.putParcelableArrayList("Rlista",reziseri);
                    fl.setArguments(argumenti);
                    argumenti.putParcelableArrayList("Zlista",zanrovi);
                    flz.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.mjestoOstalo,fl,"Lista").addToBackStack(null).commit();
                    fm.beginTransaction().replace(R.id.mjestoGlumci,flz,"Lista").addToBackStack(null).commit();
                }
                else
                {

                    ArrayList<Reziser> reziseri = FragmentDetaljiGlumac.listaRezisera;
                    ArrayList<Zanr> zanrovi=FragmentDetaljiGlumac.listaZanrova;
                    argumenti.putParcelableArrayList("Rlista",reziseri);
                    fl.setArguments(argumenti);
                    argumenti.putParcelableArrayList("Zlista",zanrovi);
                    flz.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.mjestoOstalo,fl,"Lista").addToBackStack(null).commit();
                    fm.beginTransaction().replace(R.id.mjestoGlumci,flz,"Lista").addToBackStack(null).commit();
                }

            }


        });



        return view;



    }
}
